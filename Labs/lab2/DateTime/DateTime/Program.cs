﻿using DateLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateTime
{
    class Program
    {
        static void Main(string[] args)
        {
            Date obj1 = new Date();
            
            Console.WriteLine("Enter the date\n ");
            Console.WriteLine("Enter Day:");
            int day = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Month:");
            int month = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Year:");
            int year = Convert.ToInt32(Console.ReadLine());
            Date obj2 = new Date { mDay = day, mMonth = month, mYear = year };
            Console.WriteLine("values from parameterized constructor are:\n" + "day-month-year\t" + day +"-"+month +"-"+ year );

        }
    }
}
