﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateLib
{
    public class Date
    {
        public int mDay { get; set; }
        public int mMonth { get; set; }
        public int mYear { get; set; }

        public Date()
        {
            Console.Write("Default date");
            mDay = 10;
            mMonth = 08;
            mYear = 2018;
            Console.WriteLine(mDay+"-"+mMonth+"-"+mYear);
        }
        public Date(int dd,int mm,int yy)
        {
            mDay = dd;
            mMonth = mm;
            mYear = yy;
        }
    }
}
