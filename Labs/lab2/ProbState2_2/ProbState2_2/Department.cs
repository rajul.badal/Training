﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProbState2_2
{
    class Department
    {
        public int DeptID { get; set; }
        public string DeptName { get; set; }
        public string DeptLoc { get; set; }
        public Department(string name,int ID, string loc)
        {
            DeptID = 1000 + ID;
            DeptName = name;
            DeptLoc = loc;
        }
     public void display()
        {
            Console.WriteLine("Department id:"+DeptID+"\n"+"department name:"+DeptName+"\n"+"department location:"+DeptLoc);
        }
    }
    
}
