﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProbState2_2
{
    class Employee
    {
        public int EmpId { get; set; }
        public string EmpName { get; set; }
        public string EmpDesig { get; set; }
        public double BasicSal { get; set; }
        double HRA, PF, GrossSal, Medical;
      

        public int DeptId { get; set; }
      
     

        public Employee(string name, int id, string desig, double bsal)
        {
             EmpName=name;
             EmpId=id;
            EmpDesig=desig;
            BasicSal=bsal;
        }

        public void Display()
        {
            Medical = 15000;
            HRA = BasicSal * (0.08);
            PF = BasicSal * (0.12);
            GrossSal = BasicSal + HRA + Medical;
            Console.WriteLine("1.Employee name:" + EmpName +"\n"+"2.Employee ID:"+EmpId+"\n"+"3.Employee designation:"+EmpDesig+"\n");
            Console.WriteLine("4.Basic salary=" + BasicSal + "\n" + "5.HRA:" + HRA + "\n" + "6.PF:" + PF + "\n" + "7.Gross sal:" + GrossSal + "\n");

        }
    }
}

