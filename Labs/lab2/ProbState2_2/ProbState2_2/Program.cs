﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProbState2_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1. Enter Employee Details\n 2. Enter Department Details\n");
            int choice = Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Console.WriteLine("Enter employee details\n");
                    Console.WriteLine("Enter Employee Name");
                    string name = Console.ReadLine();
                    Console.WriteLine("Enter Employee ID:");
                    int id = Convert.ToInt32(Console.ReadLine());
                    
                    Console.WriteLine("Enter employee designation(Employee,manager,president)");
                    string desig = Console.ReadLine();
                    Console.WriteLine("Enter basic salary:");
                    double bsal = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Enter 1 to display details ,2 to exit\n");
                    int input = Convert.ToInt32(Console.ReadLine());
                    if(input==1)
                    {
                        
                        Employee obj2 = new Employee(name, id, desig, bsal);
             
                        obj2.Display();
                    }
                    else
                    {
                        Console.WriteLine("exiting...");

                    }
                    break;
                case 2:
                    {
                        Console.WriteLine("Enter department details\n");
                        Console.WriteLine("Enter department name:\n");
                        string deptname = Console.ReadLine();
                        Console.WriteLine(  "Enter department id:\n");
                        int deptID = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Enter department location:\n");
                        string loc = Console.ReadLine();
                        Department obj2 = new Department(deptname, deptID, loc);
                        obj2.display();
                       
                    }
                    break;
                default:
                    Console.WriteLine("CHOSE EITHER 1 OR 2");
                    break;
                    

            }
            Console.ReadLine();
        }
    }
}
