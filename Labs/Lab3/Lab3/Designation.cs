﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    class Designation
    {
        
        public string EmpDesig { get; set; }
        public int EmpSal { get; set; }
        public Designation(int sal,string name)
        {
            EmpSal = sal;
            EmpDesig = name;
        }
        publics double RetSal(int sal,double PF,double HRA,double GrossSal)
        {
            Console.WriteLine("salary for an employee\n");
            PF = sal * (10 / 100);
            Console.WriteLine("PF(10%):"+PF);
            HRA = sal * (08 / 100);
            Console.WriteLine("HRA(8%)"+HRA);
            GrossSal = sal - PF + HRA;
            Console.WriteLine("total(salary+hra-pf):"+GrossSal);
            return GrossSal;
        }
        public  double RetSal(int sal, double PF, double HRA, double GrossSal,int Medical)
        {
            Console.WriteLine("Calculating salary for manager designation");
            Medical = 15000;
            Console.WriteLine("Medical allowance:"+Medical);
            PF = sal * (12 / 100);
            Console.WriteLine("PF(12%):"+PF);
            HRA = sal * (08 / 100);
            Console.WriteLine("HRA(8%):" + HRA);
            GrossSal = sal - PF + HRA+Medical;
            Console.WriteLine("TOTAL SALARY(HRA+MEDICAL+BASIC-PF):"+GrossSal);
            return GrossSal;
        }
        public  double RetSal(int sal, double PF, double GrossSal, int Medical)
        {
            Console.WriteLine("Calculating salary for president designation\n");
            Medical = 30000;
            Console.WriteLine("Medical allowance:"+Medical);
            PF = sal * (15/ 100);
            Console.WriteLine("PF(15%):"+PF);
            GrossSal = sal + PF  + Medical;
            Console.WriteLine("Salary for president(MEDICAL+BASIC-PF):"+GrossSal);
            return GrossSal;
            
        }
    }
}
