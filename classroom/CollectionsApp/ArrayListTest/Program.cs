﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ArrayListTest
{
    class Program
    {

        private static readonly string[] colors = { "MAGENTA", "RED", "WHITE", "CYAN","BLUE" };
        private static readonly string[] removeColors = { "RED", "WHITE", "BLUE" };
        static void Main(string[] args)
        {
            ArrayList list = new ArrayList(1);
            Console.WriteLine("Base capacity:",+list.Capacity);
            //add elements from static array into this  list
            foreach ( var color in colors)
            {
                list.Add(color); 

            }
            Console.WriteLine("capacity:" +list.Capacity);
            ArrayList removeList = new ArrayList(removeColors);
            DisplayInfo(list);
            //remove colors
            removeColorrs(list,removeList);
            Console.WriteLine("Array list after removing");
            DisplayInfo(list);
            Console.ReadLine();


        }

        private static void removeColorrs(ArrayList list, ArrayList removeList)
        {
            for (int count = 0; count < removeList.Count; count++)
            {
                list.Remove(removeList[count]);

            }
        }
  

        private static void DisplayInfo(ArrayList list)
        {
            // ITERATE THROUGH LIST
            foreach (var item in list)
            {
                Console.Write("{0}\t",item);
            }
            Console.WriteLine("\nSize:{0};capacity:{1}", list.Count, list.Capacity);
            int index = list.IndexOf("BLUE");
            if (index!=-1)

            {
                Console.WriteLine("The array list contains BLUE at index {0}",index);
            }
            else
            {
                Console.WriteLine("The array list does not contain blue anywhere");
            }
        }
    }
}
