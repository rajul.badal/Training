﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace CollectionsApp
{
    class Program
    {
        private static int[] intValues = { 1, 2, 3, 4, 5, 6 };
        private static double[] doublevalues = { 8.4, 9.3, 0, 2, 7.9, 3, 4 };
        private static int[] intValuesCopy;
        static void Main(string[] args)
        {
            //1.Array
            intValuesCopy = new int[intValues.Length];
            //initialitizing a private varialble that we have declared as class label.
            Console.WriteLine("Initial array values:\n");
            PrintArrays();

            // sort doublevalues. sort is a static method of array class
            Array.Sort(doublevalues);

            // Copy values from intValues to intValuesCopy array
             Array.Copy(intValues, intValuesCopy, intValuesCopy.Length);
            Console.WriteLine("\n Arrays after sort and copy:\n");
            PrintArrays();
            // search 5 in intValues Array
            int result = Array.BinarySearch(intValues, 5);
            if (result>=0)
            {
                Console.WriteLine("5 found at element {0} in intValues",result);
            }
            else
            {
                Console.WriteLine("5 not found in int values");
            }
            //search 8783 in intValues
            int result2 = Array.BinarySearch(intValues, 8783);
            if (result>=0)
            {
                Console.WriteLine("8783 found at element {0}",result2);
            }
            else
            {
                Console.WriteLine("element 8783 not found");
            }
            Console.ReadLine();
        }

        private static void PrintArrays()
        {
            Console.WriteLine("doubleValues");
            IEnumerator enumerator = doublevalues.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Console.WriteLine(enumerator.Current+"");

            }
            Console.WriteLine("\nintValues:");
            enumerator = intValues.GetEnumerator();
            while (enumerator.MoveNext())

            {
                Console.Write(enumerator.Current+"");
            }
            Console.WriteLine("\nintValuesCopy:");
            foreach (var element in intValuesCopy)
            {
                Console.WriteLine(element+"");
            }
            Console.WriteLine();
        }
    }
}
