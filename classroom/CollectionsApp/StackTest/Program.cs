﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace StackTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack stack = new Stack();
            bool aboolean = true;
            char aChar = '$';
            int aInt = 34657;
            string aString = "hello";

            //use push
            stack.Push(aboolean);
            PrintStack(stack);
            stack.Push(aChar);
            PrintStack(stack);
            
            stack.Push(aInt);
            PrintStack(stack);
            stack.Push(aString);
            PrintStack(stack);
            product p = new product { ID = 100 };
            stack.Push(p);
            PrintStack(stack);
       
            Console.WriteLine("First item in stack is {0}\n",stack.Peek());
            PrintStack(stack);
            object o = stack.Pop();
            product prd = (product)o;
            Console.WriteLine(prd.ID);
            PrintStack(stack);
            Console.ReadLine();
        }

        private static void PrintStack(Stack stack)
        {
            if (stack.Count==0)

            {
                Console.WriteLine("empty:");
            }
            else
            {
                Console.WriteLine("stack is:");
                foreach (var item in stack)
                {
                    Console.WriteLine("{0}",item);
                }
            }
        }
    }
}
