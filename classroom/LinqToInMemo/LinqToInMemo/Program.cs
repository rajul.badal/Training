﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqToInMemo
{
    class Program
    {
        static void Main(string[] args)
        {
            ObjectQuery();
            Console.ReadLine();
        }
        /*static void NumQuery()
        {
            var numbers = new int[] { 1, 4, 9, 16, 25, 35 };
            var evenNumbers = select c from ;
            Console.WriteLine("result:");
            foreach (var item in evenNumbers)
            {
                Console.WriteLine(item);
            }
            {

            }
        }*/
        static IEnumerable<Customer> CreateCustomers()
        {
            return new List<Customer>
            {
                new Customer{CustID="ABCD",City="Berlin"},
                new Customer{CustID="EFGH",City="London"},
                new Customer{CustID="IJKL",City="London"},
                new Customer{CustID="MONP",City="New york"},
                new Customer{CustID="QRST",City="Berlin"},
                new Customer{CustID="UVXY",City="New york"},
                new Customer{CustID="ZORO",City= "London"},
                new Customer{CustID="NATA",City="Mumbai"},

            };
            
        } static void ObjectQuery()
        {
            var result = from c in CreateCustomers()
                         where c.City == "London"
                         select c;
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
        }
    }
}
