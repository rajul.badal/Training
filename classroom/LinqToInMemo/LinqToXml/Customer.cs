﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqToXml
{
    class Customer
    {
        public string CustID { get; set; }
        public string City { get; set; }
        public override string ToString()
        {
            return CustID + "\t" + City;
        }
    }
}
