﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LinqToXml
{
    class Program
    {
        static void Main(string[] args)
        {
            //ObjectQuery();
           
            XMLQuery();
            Console.ReadLine();
        }
        static IEnumerable<Customer> CreateCustomers()
        {
            return from c in XDocument.Load("Customers.xml").Descendants("Customers").Descendants()
                   select new Customer { City = c.Attribute("City").Value,
                       CustID = c.Attribute("CustID").Value };
            //{
            //    new Customer{CustID="ABCD",City="Berlin"},
            //    new Customer{CustID="EFGH",City="London"},
            //    new Customer{CustID="IJKL",City="London"},
            //    new Customer{CustID="MONP",City="New york"},
            //    new Customer{CustID="QRST",City="Berlin"},
            //    new Customer{CustID="UVXY",City="New york"},
            //    new Customer{CustID="ZORO",City= "London"},
            //    new Customer{CustID="NATA",City="Mumbai"},

            //};

        }
        static void ObjectQuery()
        {
            var result = from c in CreateCustomers()
                         where c.City == "London"
                         select c;
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
        }
        static void XMLQuery()
        {
            var doc = XDocument.Load("Customers.xml");
            var result = from c in doc.Descendants("Customer") where c.Attribute("City").Value == "London"
                         select c;
            XElement transformDoc = new XElement("Londoners", from Customer in result select new XElement("Contract",
                new XAttribute("ID",Customer.Attribute("CustID").Value),
                 new XAttribute("Name", Customer.Attribute("ContractName").Value),
                  new XAttribute("City", Customer.Attribute("City").Value)));
          
            Console.WriteLine("Result:{0}\n",transformDoc);
            transformDoc.Save("transform.xml");


            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}
        }
    }
   
}
