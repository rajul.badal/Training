﻿using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Directory information program. 
namespace FileIO
{
    class Program
    {
        static void Main(string[] args)
        {
            //which directory info you want
            DirectoryInfo di = new DirectoryInfo(@"c:\Windows");
            
            Console.WriteLine("Listing contents of {0} directory",di.FullName);
            foreach (DirectoryInfo file in di.GetDirectories())
            {
                Console.WriteLine("Directory Name:"+di.Name);
                foreach (var file in di.GetFiles())
                {
                    Console.WriteLine("\t"+file.Name);
                }
                //list details of every file
                //Console.WriteLine("File Name:" + file.Name);
                //Console.WriteLine("Size (bytes)"+file.Length);
                //Console.WriteLine("Creation Time:"+file.CreationTime);
                Console.WriteLine();

            }
            Console.ReadLine();
        }
    }
}
