﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace SerializationApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // create book.txt to store all book information
            FileStream fs = File.OpenWrite(@"c:\Training\books.txt");
            //create formatter for storing books in books.txt
            BinaryFormatter bf = new BinaryFormatter();
            book b1 = new book { BookID = 100, BookName = "Intro to C#", Price = 245 };
            b1.SetPublisher("Packect");
            book b2 = new book { BookID = 101, BookName = "PRO to C#", Price = 550 };
            b2.SetPublisher("APress");
            List < book > b= new List<book> { b1, b2 };
            bf.Serialize(fs, b);


            fs.Close();
            Console.WriteLine("Book stored in books.txt file!");
            fs = File.OpenRead(@"c:\Training\books.txt");
            List<book> books = (List<book>)bf.Deserialize(fs);

         
            fs.Close();
            foreach (var item in books)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();



        }
    }
}
