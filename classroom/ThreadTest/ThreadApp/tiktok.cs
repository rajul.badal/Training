﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadApp
{
    public class tiktok
    {
        public void Tick(bool running)
        {
            lock(this)
            {
                if(!running)
                {
                    // Notify any waiting threads.
                    Monitor.Pulse(this);
                    return;
                }
                Console.WriteLine("Tick");
                //let tock() to run
                Monitor.Pulse(this);
                //wait tick() to complete or notify
                Monitor.Wait(this);
            }

        }
        public void Tock(bool running)
        {

            lock (this)
            {
                if (!running)
                {
                    // Notify any waiting threads.
                    Monitor.Pulse(this);
                    return;
                }
                Console.WriteLine("Tock");
                //let tick() to run
                Monitor.Pulse(this);
                //wait tick() to complete or notify
                Monitor.Wait(this);
            }
        }
    }
}
