﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
namespace ThreadApp
{
    public class Printer
    {
        //char Ch;
        //int SleepTime;
        //string objName;
        //public Printer(char c, int t, string n)
        //{
        //    this.Ch = c;
        //    this.SleepTime = t;
        //    this.objName = n;
        //}
        public void Print(object message)
        {
            //acquire a lock
            Monitor.Enter(this);
            try
            {
                Console.Write("***" + message);
                Thread.Sleep(5000);
                Console.WriteLine("***");

            }
            catch(Exception)
            {
                //Exception handling goes here
            }
            finally
            {
                Monitor.Exit(this);
            }

            //Console.WriteLine("Print() of {0} is on thread number {1}",this.objName,Thread.CurrentThread.ManagedThreadId);
            //for (int i = 0; i < 100; i++)
            //{
            //    Console.WriteLine(Ch);
            //    Thread.Sleep(SleepTime);
            //}
        }
    }
}
