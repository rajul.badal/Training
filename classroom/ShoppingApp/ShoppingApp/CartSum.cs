﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp
{
   public class CartSum: ICartInterface // class cartsum implements ICartInterface
    {
    
        public void PrintCartDetails()
        {
            double grandtot = 0;
            var db = new DataClasses1DataContext();
            var result = from c in db.Carts

                         select c;
            Console.WriteLine("Cart Summary:-\n");
            int count = 1;
            foreach (var item in result)
            {

                Console.WriteLine(count + "  Product ID:{0}\tProduct Name:{1}" +
                        "\t Product Price:{2}\tProduct Quantity:{3}\tProduct SubTotal:{4}\n ", item.Id, item.Name,
                        item.Price, item.Quantity, item.Sub_Total);
                grandtot += item.Sub_Total;
                count++;
            }

            Console.WriteLine("\nTotal items in cart:{0}\n", db.Carts.Count()); 

            Console.WriteLine("\nTotal cart price:{0}\n", grandtot);
        }
    }
}
