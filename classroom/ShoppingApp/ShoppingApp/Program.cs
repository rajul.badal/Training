﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Collections;
using System.Xml.Linq;

namespace ShoppingApp
{
    class Program
    {
      
        static void Main(string[] args)

        {
            ICartInterface ict;
            Console.WriteLine("Welcome to E-Shopping Application \n ");
           

            DataDetails(); // calling function to display product details

            bool select = true;
            while (select)
            {
                Console.WriteLine("\n1.Add Products in cart " +
                               "\n2.To veiw your Cart \n3.Update Quantity\n4.Remove a product\n" +
                               "5.See summary of cart\n6.Exit");
                try
                {
                    int choice = Convert.ToInt32(Console.ReadLine());
                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine("Enter name of the product you want to add:\n");
                            string input1 = Console.ReadLine();
                            Console.WriteLine("Enter Quantity of {0}:\n", input1);
                            int qty = Convert.ToInt32(Console.ReadLine());

                            var db = new DataClasses1DataContext(); // creating instance of datacontext class. Main entry point for LINQ to SQL framework.
                            var result = (from p in db.Products  // query to select the product details from database 
                                          where p.Name == input1
                                          select p).SingleOrDefault();
                            Cart obj = new Cart(); // instance of class Cart 
                            obj.Name = result.Name;
                            obj.Price = (double)result.Price;
                            obj.Quantity = qty;
                            obj.Sub_Total = qty * obj.Price;
                            db.Carts.InsertOnSubmit(obj);
                            db.SubmitChanges();
                            Console.WriteLine("{0} added to cart!\n", obj.Name);
                            break;
                        case 2:
                            ict = new CartDet(); // instance of interface ICartInterface calling printCartDetails method from CartDet class.
                            ict.PrintCartDetails();
                            break;

                        case 3:
                            ict = new CartDet();
                            ict.PrintCartDetails();
                         
                            CartDet cd = new CartDet();
                            cd.Update();
                            break;
                        case 4:
                            ict = new CartDet();
                            ict.PrintCartDetails();
                          

                            CartDet cd2 = new CartDet();
                            cd2.RemovePro();
                            break;
                        case 5:
                            ict = new CartSum(); //interface from class CartSum calling method PrintCartDetails 
                            ict.PrintCartDetails();
                            break;
                        case 6:
                            Environment.Exit(0);
                            break;
                        default:



                            break;

                    }
                }
                catch (FormatException) //The exception that is thrown when the format of an argument is invalid,
                                          // or when a composite format string is not well formed.
                {

                    Console.WriteLine("Invalid Input ! please try again ");
                }
             }
            Console.ReadLine();
        }
        static void DataDetails()
        {
            // program to display available products category wise
            HashSet<string> h1 = new HashSet<string>(); 
            var db1 = new DataClasses1DataContext();

            var result = from p in db1.Products
                         orderby p.Category
                         select p;
            foreach (var item in result)
            {
                h1.Add(item.Category); // Adding category to hashset

            }
            for (int i = 0; i < h1.Count(); i++)
            {

                string str = h1.ElementAt(i);
                var result4 = from d in db1.Products // fetching details of product category wise
                             where d.Category ==str
                             select d;
                Console.WriteLine("~~~~~~~~~~~~ \t Category:{0}\t ~~~~~~~~~~~~",str);
                foreach (var item3 in result4)
             {

                        Console.WriteLine("\n ID:{0}\n Name:{1}\n Description:{2}\n Category:{3}\n Price:{4}\n", item3.Id, item3.Name,
                                            item3.Description, item3.Category, item3.Price);
            }

               
            }
            Console.WriteLine("----------------- Please select from the menu----------------------------------------");

        }

       
    }

}

























               
              

    

