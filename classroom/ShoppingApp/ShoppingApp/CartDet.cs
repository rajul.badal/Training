﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Xml.Linq;
using System.Data.Linq;

namespace ShoppingApp
{
    public class CartDet:ICartInterface // class cartdet implements ICartInterface
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double SubTotal { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }



        public void PrintCartDetails()

        {

            var db = new DataClasses1DataContext();
            var result = from c in db.Carts
                         select c;
            if (db.Carts.Count() == 0)
            {
                Console.WriteLine("Cart is empty!");
            }
            else
            {
                int count = 1;
                Console.WriteLine("Cart Details are:\n");
                foreach (var item in result)
                {
                    Console.WriteLine(count+".\n"+"-Product ID:{0}\n-Product Name:{1}\n" +
                        "-Product Price:{2}\n-Product Quantity:{3}\n-Product SubTotal:{4}\n ", item.Id, item.Name,
                        item.Price, item.Quantity, item.Sub_Total);
                    count++;
                }
                Console.WriteLine("-------------------------------------------");
            }
        }
        public void Update() 
        {


            var db = new DataClasses1DataContext();

            if (db.Carts.Count() == 0)
            {
                Console.WriteLine("\n Please add  an item first");
            }
            if (db.Carts.Count() != 0)
            {

                Console.WriteLine("Enter ID of the product to update quantity:\n");
                int ProId = Convert.ToInt32(Console.ReadLine());
             
                var result = from c in db.Carts
                             where c.Id ==ProId
                         select c;
                foreach (var item in result)
                {
                    Console.WriteLine("Enter new quantity:\n");
                    int qty = Convert.ToInt32(Console.ReadLine());
                    item.Quantity = qty;
                    item.Sub_Total = qty * item.Price;

                    Console.WriteLine("\nUpdated  to quantity:{0} and new subtotal is:{1}", item.Quantity, item.Sub_Total);
                }
            }
            db.SubmitChanges(); // submit the changes to reflect in database
            Console.WriteLine("-----------------------------");
        }
        public void RemovePro()
        {
            var db = new DataClasses1DataContext();

            if (db.Carts.Count() == 0)
            {
                Console.WriteLine("Nothing to remove");
            }
            if (db.Carts.Count() != 0)

            {
                Console.WriteLine("Enter ID of the product you want to remove:\n");
                int ProId2 = Convert.ToInt32(Console.ReadLine());
           
                var result = from c in db.Carts
                             where c.Id == ProId2
                             select c;
               
                foreach (var item in result)
                {


                    db.Carts.DeleteOnSubmit(item);// puts the elements into a pending delete state
                        Console.WriteLine("Deleted from cart!\n");
                   
                }
            }
            db.SubmitChanges();
            Console.WriteLine("--------------------------------------------");
        }

      
    }

}
