﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Account a = new Account(5000);
                Console.WriteLine("Current Balance:{0}", a.Balance);
                a.deposit(10000);
                Console.WriteLine("New balance:{0}", a.Balance);
                a.withdraw(100000);
                Console.WriteLine("Currenrt balance:{0}", a.Balance);
            }
            catch (InsufficientBalanceException ibe)
            {
                Console.WriteLine(ibe.Message);
                
            }
        }
    }
}
