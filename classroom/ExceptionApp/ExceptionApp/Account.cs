﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionApp
{
    class Account
    {
        private int balance;
        public Account(int amount)
        {
            this.balance = amount;
        }
        public void withdraw(int amt)
        {
            if (this.balance<amt)
            {
                //throw insufficient balance exception
                throw new InsufficientBalanceException("Not enough balance");

            }
            this.balance -= amt;
        }
        public void deposit(int amt)
        {
            this.balance += amt;
        }
        public int Balance
        {
            get
            {
                return this.balance;
            }
        }
    }
}
