﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
namespace TPLApp
{
    class Program
    {
        static void Main(string[] args)


        {
            var t1 = Task<int>.Factory.StartNew(() => GenerateNumbers());
            var t2 = Task<string>.Factory.StartNew(() => PrintCharacters());
            var t3 = Task<int>.Factory.StartNew(() => PrintArray());
            Console.WriteLine("Numbers:{0}",t1.Result);
            Console.WriteLine("Characters:{0}", t2.Result);
            Console.WriteLine("Array:{0}", t3.Result);




            //Task<int> t1 = new Task<int>(GenerateNumbers);
            //Task<int> t2 = new Task<int>(PrintCharacters);
            //Task<int> t3 = new Task<int>(PrintArray);

            //Thread t1 = new Thread(new ThreadStart(GenerateNumbers));
            //Thread t2 = new Thread(new ThreadStart(PrintCharacters));
            //Thread t3 = new Thread(new ThreadStart(PrintArray));
            //Console.WriteLine("Threads started at:{0}",DateTime.Now.ToString());
            //t1.Start();
            //t2.Start();
            //t3.Start();

            //t1.Join();
            //t2.Join();
            //t3.Join();
            //Console.WriteLine("Threads ended at:{0}", DateTime.Now.ToString());
            //Console.WriteLine("Using TPL..........");
            //Console.WriteLine("TPL Started at:{0}",DateTime.Now.ToString());
            //Parallel.Invoke(
            //    new Action(GenerateNumbers),new Action(PrintCharacters),new Action(PrintArray)
            //    );
            //Console.WriteLine("TPL ended at:{0}", DateTime.Now.ToString());
            Console.ReadLine();
        }
        static int GenerateNumbers()
        {
            
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("GenerateNumbers()-Number:{0}",i);
                Thread.Sleep(1000); 
            }
         
            return i;
        }
        static string PrintCharacters()
        {

            string str = "My Name is Natrajan";
            for (int i = 0; i < str.Length; i++)
            {
                Console.WriteLine("Print characters()-Character:{0}",str[i]);
                Thread.Sleep(1000);
            }
            return str;
        }
        static int PrintArray()
        {
            int[] arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("PrintArray()-Array:{0}",arr[i]);
                Thread.Sleep(1000);
            }
            return arr.Count();
        }
    }

}
