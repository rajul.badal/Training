﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeApp
{
    class Program
    {
        static void Main(string[] args)
        {
            DoorBell bell = new DoorBell();
            bell.RingBell += OpenTheDoor;
            bell.RingtheBell();
            Console.ReadLine();
        }

        private static void OpenTheDoor(string cabinName) 
        {
            Console.WriteLine("Door opened");
            Console.WriteLine("This is {0} ",cabinName);
        }
    }
}
