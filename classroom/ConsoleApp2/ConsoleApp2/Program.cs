﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//DELEGATES
namespace ConsoleApp2
{
    delegate int Compute(int a);
    


    class Program
    {
        static void Main(string[] args)
        {
            //Compute c = new Compute(Square);  c# 2.0 way of creating object
            Compute c = Square; //c# 3.0 way
            var result= c.Invoke(13);                 //Method is invoking square method which is registered with delegate object
            Console.WriteLine("Result is:{0}",result);
            Console.ReadLine();
        }
        static int Square(int x)
        {
            return x * x;
        }
    }
   
}
