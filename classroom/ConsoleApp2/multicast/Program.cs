﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace multicast
{
    delegate void compute(int a);
    class Program
    {
        static void Main(string[] args)
        {
            compute c = Cube;
            c += Square;
            c.Invoke(13);

            Console.ReadLine();
        }
        static void Square(int x)
        {
            Console.WriteLine("Square of {0} is {1}", x, (x * x));
        }
        static void Cube(int y)
        {
            Console.WriteLine("cube of {0} is {1}", y, (y*y*y));
        }
    }
}
