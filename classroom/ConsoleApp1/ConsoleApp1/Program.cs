﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            DrawPolygon(10, 12);
            DrawPolygon(10, 12, 13);
            Console.ReadLine();
        }
        static void DrawPolygon(params int[] points)
        {
            Console.WriteLine("A polygon of {0} sides",points.Length);
        }
    }
}
