﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Printers avail");
            Console.WriteLine("1.laser");
            Console.WriteLine("2.dot mat");
            Console.WriteLine("enter choice");
            int choice = Convert.ToInt32(Console.ReadLine());
            IPrinter printer = null;
            switch (choice)
            {
                case 1:
                    printer = new LaserPrinter();
                    printer.Print();
                    break;
                case 2:
                    printer = new DotMatrix();
                    printer.Print();
                    break;
                default:
                    Console.WriteLine("please enter either 1 or 2 as your choice..");
                    break;
            }
            Console.ReadLine();

        }
    }
    public interface IPrinter
    {
          void Print();

      
    }
    public class LaserPrinter : IPrinter
    {
        public void Print()
        {
            Console.WriteLine("Laser print");
        }
    }
    public class DotMatrix : IPrinter
    {
        public void Print()
        {
            Console.WriteLine("mat dot print");
        }
    }

}
